defmodule Teleport.RegistrationController do
  use Teleport.Web, :controller

  alias Authy.User

  def index(conn, _params) do
    changeset = User.changeset %User{}
    render conn, changeset: changeset
  end
end
