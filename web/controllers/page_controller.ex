defmodule Teleport.PageController do
  use Teleport.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
