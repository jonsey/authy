ExUnit.start

Mix.Task.run "ecto.create", ~w(-r Teleport.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r Teleport.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(Teleport.Repo)

