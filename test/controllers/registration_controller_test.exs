defmodule Teleport.RegistrationControllerTest do
  use Teleport.ConnCase

  test "GET /signup", %{conn: conn} do
    conn = get conn, "/signup"
    assert html_response(conn, 200) =~ "Signup"
  end

  test "Signup has field for email", %{conn: conn} do
    conn = get conn, "/signup"
    response = html_response(conn, 200)
    assert response =~ "Email"
    assert response =~ "Password"
    assert response =~ "Confirm"
  end

  test "Should provide User changeset" do
    conn = get conn, "/signup"
    response = html_response(conn, 200)
    assert conn.assigns("changeset") == "User"
  end
end
